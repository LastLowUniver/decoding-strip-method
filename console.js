import { ConsoleHandler } from "./console/ConsoleHandler.js";

const alphabet = [
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    'abcdefghijklmnopqrstuvwxyz',
    'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ',
    'абвгдеёжзийклмнопрстуфхцчшщъыьэюя',
    'Свой вариант'
];

export const getData = async () => {
    const rl = new ConsoleHandler();

    let alphabetChoice = await rl.getInChoices(alphabet);
    alphabetChoice = alphabetChoice === 4
        ? await rl.getAnswer('Ваш вариант: ')
        : alphabet[alphabetChoice]

    const cryptogram = await rl.getAnswer('Криптограмма: ')
    const numStruct = await rl.getAnswer('Количество полосок (8): ', 8)

    return {
        alphabetChoice,
        cryptogram,
        numStruct
    }
}