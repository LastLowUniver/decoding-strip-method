import readline from "readline";

export class ConsoleHandler {

    constructor() {}

    async getAnswer(question, withOutAnswer) {
        return new Promise((resolve, rejects) => {
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });
    
            rl.question(question, (answer) => {
                rl.close()

                if (answer === "") {
                    resolve(withOutAnswer)
                }

                resolve(answer)
            });
        })
    }

    async getInChoices(choices) {
        return new Promise((resolve, rejects) => {
            const displayChoices = () => {
                console.log('Выберите вариант: ');
    
                for (let i = 0; i < choices.length; i++) {
    
                    const arrow = i === selectedIndex ? '➜' : ' ';
                    console.log(`${arrow} ${choices[i]}`);
                }
            }
    
            const handleKeyPress = (key) => {
                if (key === '\u001B\u005B\u0041' && selectedIndex > 0) {
                    selectedIndex--;
                    this.#clearLines(choices.length + 1);
                    displayChoices();
                } else if (key === '\u001B\u005B\u0042' && selectedIndex < choices.length - 1) {
                    selectedIndex++;
                    this.#clearLines(choices.length + 1);
                    displayChoices();
                }
            }
    
            const handleInput = (input) => {
    
                console.log(`Выбрано: ${choices[selectedIndex]}`);
    
                rl.close();
                resolve(selectedIndex);
            }

            let selectedIndex = 0;
    
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });
    
            rl.input.on('data', key => handleKeyPress(key.toString()));
    
            displayChoices();
    
            rl.question('', handleInput);    
        })
    }
    
    #clearLines(count) {
        for (let i = 0; i < count; i++) {
            process.stdout.write('\x1B[2K\x1B[A');
        }
    }
}