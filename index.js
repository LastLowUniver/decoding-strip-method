import { getData } from "./console.js";

(async () => {
    const {
        alphabetChoice,
        cryptogram,
        numStruct
    } = await getData()

    for (let shift = 1; shift <= 26; shift++) {
        let decryptedWord = "";
    
        for (let i = 0; i < numStruct; i++) {
            const letter = cryptogram[i];
            const isUpperCase = letter === letter.toUpperCase();
            const index = alphabetChoice.indexOf(letter.toUpperCase());
    
            if (index !== -1) {
                const newIndex = (index - shift + 26) % 26;
                const decryptedLetter = alphabetChoice[newIndex];
    
                decryptedWord += isUpperCase ? decryptedLetter : decryptedLetter.toLowerCase();
            } else {
                decryptedWord += letter;
            }
        }
    
        console.log(`Shift ${shift}: ${decryptedWord}`);
    }
})()

